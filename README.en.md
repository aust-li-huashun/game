# game

#### Project Description

Meituan vs Hungry mini-game ("go four chess" game revamped)

#### Game rules

1. Click on each position in the board, the state will be reversed. The rule is to click on the position you want to move to first, and then click on the position before the move is considered a move
2. When pieces on a line are close together, the side with the fewest number of pieces is erased
3. If there is only one piece left, it is considered a loss

#### Main features

1. State flip function: Each position of the board can be flipped (picture change)
2. Game restart function: all pieces return to their original position
3. Game record function: the winning player gets points
4. Record reset function: points are cleared

Translated with www.DeepL.com/Translator (free version)